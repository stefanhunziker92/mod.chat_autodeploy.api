#! /bin/bash

function initialize_worker() {
    printf "***************************************************\n\t\tSetting up host \n***************************************************\n"
    # Update packages
    echo ======= Updating packages ========
    sudo apt-get update

    # Export language locale settings
    #echo ======= Exporting language locale settings =======
    #export LC_ALL=C.UTF-8
    #export LANG=C.UTF-8

    # Install pip3
    #echo ======= Installing pip3 =======
    #sudo apt-get install -y python3-wheel
    #sudo apt-get install -y  gcc libpq-dev

    # Install virtualenv
    echo ======= Installing virtualenv =======
    sudo apt-get -y install python3-venv
}


function clone_app_repository_af() {
    printf "***************************************************\n\t\tFetching App \n***************************************************\n"
    # Clone and access project directory
    echo ======== Cloning and accessing project directory ========
    if [[ -d ~/api_af ]]; then
        sudo rm -rf ~/api_af
        git clone -b master https://gitlab.com/stefanhunziker92/api_af ~/api_af
        cd ~/api_af/
    else
        git clone -b master https://gitlab.com/stefanhunziker92/api_af ~/api_af
        cd ~/api_af/
    fi
}


function setup_python_venv_af() {
    printf "***************************************************\n\t\tSetting up Venv \n***************************************************\n"

    # Create virtual environment and activate it
    echo ======== Creating and activating virtual env =======
    python3 -m venv env_af
    source ./env_af/bin/activate

    # install wheel here because in requirements.txt it will fail
    echo ======== Installing wheel =======
    pip3 install wheel

    # install gunicorn via pip, using sudo-apt will lead to errors
    echo ======== Installing wheel =======
    pip3 install gunicorn
}



function install_depencies_af() {
    printf "***************************************************\n    Installing App dependencies\n***************************************************\n"
    # Install required packages
    echo ======= Installing required packages ========
    pip install -r requirements.txt
}


function add_autodeploy_af(){
    printf "***************************************************\n    creating File for automated deploy after reboot \n***************************************************\n"
    sudo chmod a+rwx /etc/systemd/system
echo "[Unit]
Description=Gunicorn instance for api_af
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/api_af
ExecStart=/home/ubuntu/api_af/env_af/bin/gunicorn -b 0.0.0.0:8000 app:app
Restart=always

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/api_af.service

    sudo systemctl daemon-reload
    sudo systemctl start api_af
    sudo systemctl enable api_af

    deactivate
}


function clone_app_repository_xf() {
    printf "***************************************************\n\t\tFetching App \n***************************************************\n"
    # Clone and access project directory
    echo ======== Cloning and accessing project directory ========
    if [[ -d ~/api_xf ]]; then
        sudo rm -rf ~/api_xf
        git clone -b master https://gitlab.com/stefanhunziker92/api_xf ~/api_xf
        cd ~/api_xf/
    else
        git clone -b master https://gitlab.com/stefanhunziker92/api_xf ~/api_xf
        cd ~/api_xf/
    fi
}


function setup_python_venv_xf() {
    printf "***************************************************\n\t\tSetting up Venv \n***************************************************\n"

    # Create virtual environment and activate it
    echo ======== Creating and activating virtual env =======
    python3 -m venv env_xf
    source ./env_xf/bin/activate

    # install wheel here because in requirements.txt it will fail
    echo ======== Installing wheel =======
    pip3 install wheel

    # install gunicorn via pip, using sudo-apt will lead to errors
    echo ======== Installing wheel =======
    pip3 install gunicorn
}



function install_depencies_xf() {
    printf "***************************************************\n    Installing App dependencies\n***************************************************\n"
    # Install required packages
    echo ======= Installing required packages ========
    pip install -r requirements.txt
}

function add_autodeploy_xf(){
    printf "***************************************************\n    creating File for automated deploy after reboot \n***************************************************\n"
    sudo chmod a+rwx /etc/systemd/system
echo "[Unit]
Description=Gunicorn instance for api_xf
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/api_xf
ExecStart=/home/ubuntu/api_xf/env_xf/bin/gunicorn -b 0.0.0.0:8001 app:app
Restart=always

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/api_xf.service

    sudo systemctl daemon-reload
    sudo systemctl start api_xf
    sudo systemctl enable api_xf
}

function clone_app_repository_my() {
    printf "***************************************************\n\t\tFetching App \n***************************************************\n"
    # Clone and access project directory
    echo ======== Cloning and accessing project directory ========
    if [[ -d ~/api_my ]]; then
        sudo rm -rf ~/api_my
        git clone -b master https://gitlab.com/stefanhunziker92/api_my ~/api_my
        cd ~/api_my/
    else
        git clone -b master https://gitlab.com/stefanhunziker92/api_my ~/api_my
        cd ~/api_my/
    fi
}


function setup_python_venv_my() {
    printf "***************************************************\n\t\tSetting up Venv \n***************************************************\n"

    # Create virtual environment and activate it
    echo ======== Creating and activating virtual env =======
    python3 -m venv env_my
    source ./env_my/bin/activate

    # install wheel here because in requirements.txt it will fail
    echo ======== Installing wheel =======
    pip3 install wheel

    # install gunicorn via pip, using sudo-apt will lead to errors
    echo ======== Installing wheel =======
    pip3 install gunicorn
}



function install_depencies_my() {
    printf "***************************************************\n    Installing App dependencies\n***************************************************\n"
    # Install required packages
    echo ======= Installing required packages ========
    pip install -r requirements.txt
}


function add_autodeploy_my(){
    printf "***************************************************\n    creating File for automated deploy after reboot \n***************************************************\n"
    sudo chmod a+rwx /etc/systemd/system
echo "[Unit]
Description=Gunicorn instance for api_my
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/api_my
ExecStart=/home/ubuntu/api_my/env_my/bin/gunicorn -b 0.0.0.0:8002 app:app
Restart=always

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/api_my.service

    sudo systemctl daemon-reload
    sudo systemctl start api_my
    sudo systemctl enable api_my

    deactivate
}

function clone_app_repository_fck_crs() {
    printf "***************************************************\n\t\tFetching App \n***************************************************\n"
    # Clone and access project directory
    echo ======== Cloning and accessing project directory ========
    if [[ -d ~/api_fck_crs ]]; then
        sudo rm -rf ~/api_fck_crs
        git clone -b master https://gitlab.com/stefanhunziker92/fckcors_api ~/api_fck_crs
        cd ~/api_fck_crs/
    else
        git clone -b master https://gitlab.com/stefanhunziker92/fckcors_api ~/api_fck_crs
        cd ~/api_fck_crs/
    fi
}


function setup_python_venv_fck_crs() {
    printf "***************************************************\n\t\tSetting up Venv \n***************************************************\n"

    # Create virtual environment and activate it
    echo ======== Creating and activating virtual env =======
    python3 -m venv env_fck_crs
    source ./env_fck_crs/bin/activate

    # install wheel here because in requirements.txt it will fail
    echo ======== Installing wheel =======
    pip3 install wheel

    # install gunicorn via pip, using sudo-apt will lead to errors
    echo ======== Installing wheel =======
    pip3 install gunicorn
}



function install_depencies_fck_crs() {
    printf "***************************************************\n    Installing App dependencies\n***************************************************\n"
    # Install required packages
    echo ======= Installing required packages ========
    pip install -r requirements.txt
}


function add_autodeploy_fck_crs(){
    printf "***************************************************\n    creating File for automated deploy after reboot \n***************************************************\n"
    sudo chmod a+rwx /etc/systemd/system
echo "[Unit]
Description=Gunicorn instance for api_fck_crs
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/api_fck_crs
ExecStart=/home/ubuntu/api_fck_crs/env_fck_crs/bin/gunicorn -b 0.0.0.0:8010 app:app
Restart=always

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/api_fck_crs.service

    sudo systemctl daemon-reload
    sudo systemctl start api_fck_crs
    sudo systemctl enable api_fck_crs

    deactivate
}














function restartServices(){
    #sudo systemctl restart nginx
    sudo systemctl daemon-reload
    sudo systemctl start api_af
    sudo systemctl enable api_af
    sudo systemctl start api_xf
    sudo systemctl enable api_xf
    sudo systemctl start api_my
    sudo systemctl enable api_my
    sudo systemctl start api_fck_crs
    sudo systemctl enable api_fck_crs
}



######################################################################
########################      RUNTIME       ##########################
######################################################################

initialize_worker
clone_app_repository_af
setup_python_venv_af
install_depencies_af
add_autodeploy_af
clone_app_repository_xf
setup_python_venv_xf
install_depencies_xf
add_autodeploy_xf
clone_app_repository_my
setup_python_venv_my
install_depencies_my
add_autodeploy_my
clone_app_repository_fck_crs
setup_python_venv_fck_crs
install_depencies_fck_crs
add_autodeploy_fck_crs
restartServices
restartServices
restartServices